import java.util.Arrays;
import java.util.LinkedList;

public class ToFromList {

	// runs in O(N) because doesnt perform insert into the avltree
	public static AvlNode fromList(int[] sortedList)
	{
		return fromListHelper(sortedList,0,sortedList.length-1);
	}
	
	public static AvlNode fromListHelper(int[] sortedList, int startIndex, int endIndex){
		if(startIndex > endIndex) return null;
		
		int midIndex = startIndex + (endIndex - startIndex) / 2;
		AvlNode a = new AvlNode(new Integer(sortedList[midIndex]));
		a.left = fromListHelper(sortedList, startIndex, midIndex-1);
		a.right = fromListHelper(sortedList, midIndex+1, endIndex);
		if(a.left == null && a.right == null)
			a.height = 1;
		else if (a.left == null)
			a.height = a.right.height - 1;
		else
			a.height = a.left.height + 1;
		
		return a;
	}
	
	public static int[] staticSortedList = {1,2,3,4,5,6,7,8,9,10};
	
	public static LinkedList<Integer> toList(AvlNode root)
	{
		LinkedList<Integer> list = new LinkedList<Integer>();
		toListHelper(root, list);
		return list;
	}
	
	public static void toListHelper(AvlNode n, LinkedList<Integer> list)
	{
		if (n.left == null && n.right == null)
			list.add((Integer)n.element);
		
		else if(n.left == null){
			list.add((Integer)n.element);
			toListHelper(n.right, list);
		}
		else if(n.right == null){
			list.add((Integer)n.element);
			toListHelper(n.left, list);
			
		}
		else{
			toListHelper(n.left, list);
			list.add((Integer)n.element);
			toListHelper(n.right, list);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("FROMLIST -> tree: " + Arrays.toString(staticSortedList));
		AvlNode n = fromList(staticSortedList);
		AvlTree t = new AvlTree(n);
		//t.printTree();
		System.out.println("Stored into AVL tree.");
		System.out.println("tree -> TOLIST: " + toList(n));
		
		
	}

}

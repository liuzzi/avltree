import java.util.Arrays;


/**
 * @author fliuzzi
 */
public class AvlTree
{
	/**
	 * Construct the tree.
	 */
	public AvlTree( )
	{
		root = null;
	}
	
	public AvlTree(AvlNode node)
	{
		root = node;
	}

	/**
	 * Insert into the tree; duplicates are ignored.
	 */
	public void insert( Comparable x )
	{
		root = insert( x, root );
	}

	/**
	 * Remove from the tree. Nothing is done if x is not found.
	 */
	public void remove( Comparable x )
	{
		System.out.println( "Sorry, remove unimplemented" );
	}

	/**
	 * Find the smallest item in the tree.
	 */
	public Comparable findMin( )
	{
		return elementAt( findMin( root ) );
	}

	/**
	 * Find the largest item in the tree.
	 */
	public Comparable findMax( )
	{
		return elementAt( findMax( root ) );
	}

	/**
	 * Find an item in the tree.
	 */
	public Comparable find( Comparable x )
	{
		return elementAt( find( x, root ) );
	}

	/**
	 * Make the tree logically empty.
	 */
	public void makeEmpty( )
	{
		root = null;
	}

	/**
	 * Test if the tree is logically empty.
	 */
	public boolean isEmpty( )
	{
		return root == null;
	}


	/**
	 * Print the tree contents in sorted order.
	 */
	public void printTree( )
	{
		if( isEmpty( ) )
			System.out.println( "Empty tree" );
		else
			printTree( root );
	}

	/**
	 * Internal method to get element field.
	 */
	private Comparable elementAt( AvlNode t )
	{
		return t == null ? null : t.element;
	}

	/**
	 * Internal method to insert into a subtree.
	 */
	private AvlNode insert( Comparable x, AvlNode t )
	{
		if( t == null )
			t = new AvlNode( x, null, null );
		else if( x.compareTo( t.element ) < 0 )
		{
			t.left = insert( x, t.left );
			if( height( t.left ) - height( t.right ) == 2 )
				if( x.compareTo( t.left.element ) < 0 )
					t = rotateWithLeftChild( t );
				else
					t = doubleWithLeftChild( t );
		}
		else if( x.compareTo( t.element ) > 0 )
		{
			t.right = insert( x, t.right );
			if( height( t.right ) - height( t.left ) == 2 )
				if( x.compareTo( t.right.element ) > 0 )
					t = rotateWithRightChild( t );
				else
					t = doubleWithRightChild( t );
		}
		else
			;  // Duplicate; do nothing
			t.height = max( height( t.left ), height( t.right ) ) + 1;
			return t;
	}

	/**
	 * Internal method to find the smallest item in a subtree.
	 */
	private AvlNode findMin( AvlNode t )
	{
		if( t == null )
			return t;

		while( t.left != null )
			t = t.left;
		return t;
	}

	/**
	 * Internal method to find the largest item in a subtree.
	 */
	private AvlNode findMax( AvlNode t )
	{
		if( t == null )
			return t;

		while( t.right != null )
			t = t.right;
		return t;
	}

	/**
	 * Internal method to find an item in a subtree.
	 */
	private AvlNode find( Comparable x, AvlNode t )
	{
		while( t != null )
			if( x.compareTo( t.element ) < 0 )
				t = t.left;
			else if( x.compareTo( t.element ) > 0 )
				t = t.right;
			else
				return t;    // Match

				return null;   // No match
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 */
	private void printTree( AvlNode t )
	{
		if( t != null )
		{
			printTree( t.left );
			System.out.println( t.element );
			printTree( t.right );
		}
	}

	/**
	 * Return the height of node t, or -1, if null.
	 */
	private static int height( AvlNode t )
	{
		return t == null ? -1 : t.height;
	}

	/**
	 * Return maximum of lhs and rhs.
	 */
	private static int max( int lhs, int rhs )
	{
		return lhs > rhs ? lhs : rhs;
	}

	/**
	 * Rotate binary tree node with left child.
	 * For AVL trees, this is a single rotation for case 1.
	 * Update heights, then return new root.
	 */
	private static AvlNode rotateWithLeftChild( AvlNode k2 )
	{
		AvlNode k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		k2.height = max( height( k2.left ), height( k2.right ) ) + 1;
		k1.height = max( height( k1.left ), k2.height ) + 1;
		return k1;
	}

	/**
	 * Rotate binary tree node with right child.
	 * For AVL trees, this is a single rotation for case 4.
	 * Update heights, then return new root.
	 */
	private static AvlNode rotateWithRightChild( AvlNode k1 )
	{
		AvlNode k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		k1.height = max( height( k1.left ), height( k1.right ) ) + 1;
		k2.height = max( height( k2.right ), k1.height ) + 1;
		return k2;
	}

	/**
	 * Double rotate binary tree node: first left child
	 * with its right child; then node k3 with new left child.
	 * For AVL trees, this is a double rotation for case 2.
	 * Update heights, then return new root.
	 */
	private static AvlNode doubleWithLeftChild( AvlNode k3 )
	{
		k3.left = rotateWithRightChild( k3.left );
		return rotateWithLeftChild( k3 );
	}

	/**
	 * Double rotate binary tree node: first right child
	 * with its left child; then node k1 with new right child.
	 * For AVL trees, this is a double rotation for case 3.
	 * Update heights, then return new root.
	 */
	private static AvlNode doubleWithRightChild( AvlNode k1 )
	{
		k1.right = rotateWithLeftChild( k1.right );
		return rotateWithRightChild( k1 );
	}

	/** The tree root. */
	public AvlNode root;

}